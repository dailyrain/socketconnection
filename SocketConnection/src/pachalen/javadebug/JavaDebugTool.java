package pachalen.javadebug;
/**
 * Java Debuging Tool<br>
 * 자바 디버깅 툴<br>
 * 2013.06.04 제작<br>
 * 순천향대학교 의료IT공학과 MSDL & Team Pachalen<br>
 * 본 프로그램은 자바 프로그래밍 환경에서 디버깅을 돕기 위해 만들어졌습니다<br>
 * @author 최선욱(Niklane)
 * @version 1.1
 */
public class JavaDebugTool {
	private static Boolean isAvailable = true;
	private Boolean isTestMode;
	private static Integer ERROR_COUNT=0, DEBUG_COUNT=0;
	private String TAG;

	/**
	 * @param TAG 는 디버깅시 로그에 표시되는 이름을 나타냅니다.
	 * @throws Exception isAvailable이 False일 경우 객체는 생성되지 않습니다.
	 */
	public JavaDebugTool(String TAG) throws Exception{
		if(isAvailable){
			this.TAG = TAG;
			isTestMode = true;
		}
		else{
			throw new Exception("생성을 취소합니다");
		}
	}

	/**
	 * @param isTestMode 는 디버그 툴의 상태를 정합니다. False일경우 기능들은 작동하지 않습니다. 
	 * @param TAG 는 디버깅시 로그에 표시되는 이름을 나타냅니다.
	 * @throws Exception isAvailable이 False일 경우 객체는 생성되지 않습니다.
	 */
	public JavaDebugTool(String TAG, boolean isTestMode) throws Exception{
		if(isAvailable){
			this.isTestMode = isTestMode;
			this.TAG = TAG;
		}
		else{
			throw new Exception("생성을 취소합니다");
		}
	}

	/**
	 * @param TAG 는 디버깅시 로그에 표시되는 이름을 나타냅니다.
	 * @param isTestMode 는 디버그 툴의 상태를 정합니다. False일경우 기능들은 작동하지 않습니다.
	 * @param isAvailable 는 디버그 툴의 유효함을 정합니다. False일경우 더이상 DebugTool객체는 생성되지 않습니다.
	 * @throws Exception isAvailable이 False일 경우 객체는 생성되지 않습니다.
	 */
	public JavaDebugTool(String TAG, boolean isTestMode, boolean isAvailable) throws Exception{
		JavaDebugTool.isAvailable = isAvailable;
		if(!isAvailable){
			throw new Exception("생성을 취소합니다");
		}
		this.isTestMode = isTestMode;
		this.TAG = TAG;
		
	}
	/**
	 * Error 메세지를 발생합니다.
	 * @param message 는 발생할 객체를 의미하며 toString을 통하여 표시됩니다.
	 */
	public void Error(Object message){
		if(isTestMode){
			System.out.println(String.format("%3d", ERROR_COUNT++) + " | Error(" + TAG + ") : " + message.toString());
		}
	}
	
	public void Alert(Object message){
		if(isTestMode){
			
		}
	}
	/**
	 * Debug 메세지를 발생합니다.
	 * @param message 는 발생할 객체를 의미하며 toString을 통하여 표시됩니다.
	 */
	public void Debug(Object message){
		if(isTestMode){
			System.out.println(String.format("%3d", DEBUG_COUNT++) + " | Debug(" + TAG + ") : " + message.toString());
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String tos = new String();
		tos += "JavaDebugTool\n [" + (TAG != null ? "TAG=" + TAG : "") + "]\n";
		tos += "isTestMode=" + isTestMode + "\n";
		tos += "ERROR_COUNT=" + ERROR_COUNT + "\n";
		tos += "DEBUG_COUNT=" + DEBUG_COUNT + "\n";

		return tos;
	}

	/**
	 * @param isAvailable the isAvailable to set
	 */
	public static void setIsAvailable(Boolean isAvailable) {
		JavaDebugTool.isAvailable = isAvailable;
	}

	/**
	 * @return the isTestMode
	 */
	public Boolean getIsTestMode() {
		return isTestMode;
	}

	/**
	 * @param isTestMode the isTestMode to set
	 */
	public void setIsTestMode(Boolean isTestMode) {
		this.isTestMode = isTestMode;
	}

}
