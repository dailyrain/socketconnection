package pachalen.socketconnection;

import java.io.Serializable;
import java.net.Socket;

/**
 * 각 객체간 통신에 사용할 클래스를 만드는데 사용하여 본 클래스를 상속하여<br>
 * 만든 클래스를 이용해 정보를 주고 받습니다.<br>
 * 2013. 6. 9. 제작<br>
 * 순천향대학교 의료IT공학과 MSDL & Team Pachalen<br>
 * @author 최선욱(Niklane)
 * @version 1.0
 *
 */
public abstract class SocketContent implements Serializable{
}
