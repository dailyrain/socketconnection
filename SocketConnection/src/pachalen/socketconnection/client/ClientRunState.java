package pachalen.socketconnection.client;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import pachalen.javadebug.JavaDebugTool;
import pachalen.socketconnection.MessageType;
import pachalen.socketconnection.SocketAct;
import pachalen.socketconnection.SocketMessage;

public class ClientRunState implements ClientState {
	private JavaDebugTool D;
	SocketClient clt = SocketClient.INST;
	
	public ClientRunState(){
		try{
			D = new JavaDebugTool("ClientWaitState");
		}catch(Exception e){}
	}
	
	@Override
	public void setUp() {
		D.Error("클라이언트가 작동중일 때는 시작할 수 없습니다");
	}

	@Override
	public void setDown() {
		try {
			clt.sock.close();
			clt.currentState = clt.clientWaitState;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void addAct(MessageType msg, SocketAct act) {
		clt.MessageTypes.put(msg, act);
		D.Debug(msg + "에 해당하는 행동이 클라이언트에 할당되었습니다. 행동 수 : " + clt.MessageTypes.size());
	}

	@Override
	public void removeAct(MessageType msg) {
		clt.MessageTypes.remove(msg);
		D.Debug(msg + "에 해당하는 행동이 클라이언트에서 제거되었습니다. 행동 수 : " + clt.MessageTypes.size());
	}

	@Override
	public void login() {
		try {
			clt.writer.writeObject(new SocketMessage(SocketMessage.MsgType.LOGIN,clt.user,"",null));
			clt.writer.flush();
			D.Debug("'"+clt.user+"'의 유저명으로 로그인을 시도합니다.");
		} catch (IOException e) {
			D.Error("로그인 중 에러발생");
		}
	}

	@Override
	public void logout() {
		try{
			clt.writer.writeObject(new SocketMessage(SocketMessage.MsgType.LOGOUT,clt.user,"",null));
			clt.writer.flush();
		}catch(IOException e){
			D.Error("로그아웃 중 에러발생");
		}
		
	}

	@Override
	public void act(SocketMessage message) {
		if(clt.MessageTypes.containsKey(message.getDetailMsgType())){
			clt.MessageTypes.get(message.getDetailMsgType()).act(message);
		}
	}

	@Override
	public ArrayList<String> makeUserList(String listSource) {
		ArrayList<String> aryUserList = new ArrayList<String>();
		String temp[] = listSource.split("/");
		for(String s : temp){
			aryUserList.add(s);
		}
		Collections.sort(aryUserList);
		return aryUserList;
	}
}
