package pachalen.socketconnection.client;

import java.io.ObjectOutputStream;
import java.util.ArrayList;

import pachalen.socketconnection.MessageType;
import pachalen.socketconnection.SocketAct;
import pachalen.socketconnection.SocketMessage;

public interface ClientState {
	void setUp();
	void setDown();
	void addAct(MessageType msg, SocketAct act);
	void removeAct(MessageType msg);
	void login();
	void logout();
	void act(SocketMessage message);
	ArrayList<String> makeUserList(String listSource);
}
