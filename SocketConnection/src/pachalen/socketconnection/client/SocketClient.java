package pachalen.socketconnection.client;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

import pachalen.javadebug.JavaDebugTool;
import pachalen.socketconnection.MessageType;
import pachalen.socketconnection.SocketAct;
import pachalen.socketconnection.SocketContent;
import pachalen.socketconnection.SocketMessage;

/**
 * 소켓통신의 클라이언트입니다.<br>
 * 싱글톤기법을 이용하여 제작되었으며 사용시에는<br>
 * SocketClient clt = SocketClient.INST;<br>
 * 를 사용해 인스턴스를 획득합니다.
 * 2013. 6. 9. 제작<br>
 * 순천향대학교 의료IT공학과 MSDL & Team Pachalen<br>
 * @author 최선욱(Niklane)
 * @version 1.0
 *
 */
public enum SocketClient {
	/**
	 * SocketClient의 Singleton Interface
	 */
	INST;
	
	JavaDebugTool D;
	ObjectInputStream reader;	// 수신용 스트림
	ObjectOutputStream writer;	// 송신용 스트림
	Socket sock;				// 서버 연결용 소켓
	String user;				// 이 클라이언트로 로그인 한 유저의 이름
	int socket;
	Thread readerThread;
	String ip;
	onListUpdate lstUpdate;
	HashMap<MessageType, SocketAct> MessageTypes = new HashMap<MessageType, SocketAct>();
	
	ClientState clientWaitState;
	ClientState clientRunState;
	ClientState currentState;
	
	/**
	 * 클라이언트를 초기화합니다.
	 * @param socket 소켓번호
	 * @param ip IP주소
	 * @param lstUpdate 리스트 업데이트시에 수행할 행동 클래스
	 */
	public void init(int socket, String ip, onListUpdate lstUpdate) {
		if(clientWaitState == null){
			this.socket = socket;
			this.ip = ip;
			this.lstUpdate = lstUpdate;
			clientWaitState = new ClientWaitState();
			clientRunState = new ClientRunState();
			currentState = clientWaitState;
			try{
				D = new JavaDebugTool("SocketClient", true, true);
			}catch(Exception e){}
			D.Debug("클라이언트가 초기화 되었습니다.");
		}
		else{
			try {
				throw new Exception("이미 초기화된 클라이언트 입니다.");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				D.Error(e);
			}
		}
	}
	
	/**
	 * 클라이언트를 서버와 연결합니다.
	 */
	public void setUp(){
		currentState.setUp();
	}
	
	/**
	 * 클라이언트의 연결을 종료합니다.
	 */
	public void setDown(){
		currentState.setDown();
	}
	
	/**
	 * 서버로부터 메세지 획득시 수행할 행동을 추가합니다.
	 * @param msg {@link MessageType}을 구현한 열거형, 메세지의 종류를 정의.
	 * @param act {@link SocketAct}을 상속한 클래스, 수행할 행동을 정의.
	 */
	public void addAct(MessageType msg, SocketAct act){
		currentState.addAct(msg, act);
	}
	
	/**
	 * 추가한 행동을 삭제합니다.
	 * @param msg {@link MessageType}을 구현한 열거형, 메세지의 종류를 정의.
	 */
	public void removeAct(MessageType msg){
		currentState.removeAct(msg);
	}
	
	/**
	 * 서버로 메세지를 전송합니다.
	 * @param msgType {@link MessageType}을 구현한 열거형, 메세지의 종류
	 * @param receiver 수신자, 비워놓거나 ""처리시 서버에 연결된 모든 클라이언트에 전송합니다.
	 * @param content {@link SocketContent}를 상속한 클래스, 보낼 메세지를 정의.
	 */
	public void sendMessage(MessageType msgType, String receiver, SocketContent content){
		if(receiver == null) receiver = "";
		
		try {
			SocketMessage message = new SocketMessage(SocketMessage.MsgType.CLIENT_MSG, user, receiver, msgType,content);
			writer.writeObject(message);
			D.Debug("서버로  메세지 전송 - " + message);
		} catch (IOException e) {
			D.Error("메세지 전송중 오류 발생 - " + e);
		}
		
	}

	/**
	 * 로그인합니다.
	 * @param username 사용할 유저이름
	 */
	public void login(String username){
		user = username;
		currentState.login();
	}
	
	/**
	 * 로그아웃합니다.
	 */
	public void logout(){
		currentState.logout();
	}
	
	/**
	 * 사용자 목록이 갱신될때마다 실행되는 콜백메소드를 포함한 인터페이스<br>
	 * 2013. 6. 9. 제작<br>
	 * 순천향대학교 의료IT공학과 MSDL & Team Pachalen<br>
	 * @author 최선욱(Niklane)
	 * @version 1.0
	 *
	 */
	public interface onListUpdate{
		void listUpdate(ArrayList<String> userList);
	}

	/**
	 * 사용자 목록이 갱신될 때 실행되는 콜백 메소드를 포함한 인터페이스를 등록한다.
	 * @param lstUpdate
	 */
	public void setLstUpdate(onListUpdate lstUpdate) {
		this.lstUpdate = lstUpdate;
	}
	
}
