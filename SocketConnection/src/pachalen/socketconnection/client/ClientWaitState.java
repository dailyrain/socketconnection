package pachalen.socketconnection.client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

import pachalen.javadebug.JavaDebugTool;
import pachalen.socketconnection.MessageType;
import pachalen.socketconnection.SocketAct;
import pachalen.socketconnection.SocketMessage;

public class ClientWaitState implements ClientState {
	private JavaDebugTool D;
	SocketClient clt = SocketClient.INST;
	
	public ClientWaitState(){
		try{
			D = new JavaDebugTool("ClientWaitState");
		}catch(Exception e){}
	}
	
	private class IncomingReader implements Runnable{
		public void run() {
				SocketMessage message;
				SocketMessage.MsgType type;
				try{
					while(true){
						message = (SocketMessage)clt.reader.readObject();
						D.Debug(message.getSender() + "으로 부터 메세지 도착 - " + message);
						type = message.getMsgType();
						switch(type){
						case LOGIN_FAILURE:
							D.Error("중복된 유저이름 입니다.");
							break;
						case LOGIN_LIST:
							clt.lstUpdate.listUpdate(clt.currentState.makeUserList(message.getSender()));
							break;
						case SERVER_MSG:
							clt.currentState.act(message);
							break;
						default:
							D.Error("서버에서 알 수 없는 메세지 도착");
							break;
						}
					}
				}
				catch(Exception e){
					D.Error("서버에서 메세지 받던 중 에러 발생");
					e.printStackTrace();
				}
		}
	}
	
	@Override
	public void setUp() {
		try {
			clt.sock = new Socket(clt.ip, clt.socket);
			clt.reader = new ObjectInputStream(clt.sock.getInputStream());
			clt.writer = new ObjectOutputStream(clt.sock.getOutputStream());
			clt.readerThread = new Thread(new IncomingReader());
			clt.readerThread.start();
			clt.currentState = clt.clientRunState; // 상태변환
			D.Debug("클라이언트가 서버에 접속되었습니다. IP : "+clt.ip+" Socket : "+clt.socket);
		} catch (Exception e) {
			D.Error("서버 접속중에 오류발생, 접속을 종료합니다.");
		}
	}

	@Override
	public void setDown() {
		D.Error("클라이언트가 대기중일 때는 멈출 수 없습니다.");
	}

	@Override
	public void addAct(MessageType msg, SocketAct act) {
		clt.MessageTypes.put(msg, act);
		D.Debug(msg + "에 해당하는 행동이 클라이언트에 할당되었습니다. 행동 수 : " + clt.MessageTypes.size());
	}

	@Override
	public void removeAct(MessageType msg) {
		clt.MessageTypes.remove(msg);
		D.Debug(msg + "에 해당하는 행동이 클라이언트에서 제거되었습니다. 행동 수 : " + clt.MessageTypes.size());
	}

	@Override
	public void login() {
		D.Error("클라이언트가 대기중일 때는 로그인 할 수 없습니다.");
	}

	@Override
	public void logout() {
		D.Error("클라이언트가 대기중일 때는 로그아웃 할 수 없습니다.");
	}

	@Override
	public void act(SocketMessage message) {
		D.Error("클라이언트가 대기중일 때는 작동할 수 없습니다.");
	}

	@Override
	public ArrayList<String> makeUserList(String listSource) {
		D.Error("클라이언트가 대기중일 때는 생성할 수 없습니다.");
		return null;
	}
}
