package pachalen.socketconnection;

/**
 * 소켓통신간 사용될 열거형 메세지 타입을 생성할때 반드시 구현해야 할 인터페이스. 특별한 메소드는 없으며 단지 형을 맞추기 위한 것 뿐.<br>
 * enum Message implements MessageType {NO_ACT, MESSAGE}; 식으로 사용한다.<br>
 * 2013. 6. 4. 제작<br>
 * 순천향대학교 의료IT공학과 MSDL & Team Pachalen<br>
 * @author 최선욱(Niklane)
 * @version 1.0
 *
 */
public interface MessageType {

}
