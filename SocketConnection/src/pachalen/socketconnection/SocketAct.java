package pachalen.socketconnection;

/**
 * 소켓통신에서 메세지에 따른 동작을 나타내기 위한 인터페이스<br>
 * class SpecificAct implements SocketAct{<br>
 *	&nbsp&nbsp&nbsp&nbsp&nbsppublic void act() {<br>
 *	&nbsp&nbsp&nbsp&nbsp&nbsp}<br>
 *	}<br>
 * 다음과 같이 사용한다.
 * 2013. 6. 4. 제작<br>
 * 순천향대학교 의료IT공학과 MSDL & Team Pachalen<br>
 * @author 최선욱(Niklane)
 * @version 1.0
 *
 */
public interface SocketAct {
	void act(SocketMessage message);
}
