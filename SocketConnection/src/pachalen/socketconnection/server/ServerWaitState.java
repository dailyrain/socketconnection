package pachalen.socketconnection.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

import pachalen.javadebug.JavaDebugTool;
import pachalen.socketconnection.MessageType;
import pachalen.socketconnection.SocketAct;
import pachalen.socketconnection.SocketMessage;

/**
 * 대기 상태의 서버를 나타내는 클래스.<br>
 * {@link ServerState}를 구현하였음.<br>
 * 2013. 6. 4. 제작<br>
 * 순천향대학교 의료IT공학과 MSDL & Team Pachalen<br>
 * @author 최선욱(Niklane)
 * @version 1.0
 */
public class ServerWaitState implements ServerState{
	private JavaDebugTool D;
	
	SocketServer serv = SocketServer.INST;
	
	public ServerWaitState(){
		try {
			D = new JavaDebugTool("ServerWaitState");
		} catch (Exception e) {}
	}

	private class ServerHandler implements Runnable{
		ServerSocket serverSocket;
		public ServerHandler(ServerSocket serverSocket) {
			this.serverSocket = serverSocket;
		}
		
		public void run(){
			try {
				while(true){
					Socket clientSocket = serverSocket.accept();
					ClientHandler clientThread = new ClientHandler(clientSocket);
					clientThread.start();
					D.Debug("클라이언트 연결됨 IP" + clientSocket.getInetAddress());
				}
			} catch (IOException e) {
				D.Error("클라이언트 연결 중 이상발생");
				D.Error(e);
			}
		}
	}
	
	private class ClientHandler extends Thread{
		String username;
		Socket sock;
		ObjectInputStream reader;
		ObjectOutputStream writer;

		public ClientHandler(Socket clientSocket){
			sock = clientSocket;
			try {
				writer = new ObjectOutputStream(clientSocket.getOutputStream());
				reader = new ObjectInputStream(clientSocket.getInputStream());
				D.Debug("클라이언트와의 통로를 열었습니다. IP" + sock.getInetAddress());
			} catch (IOException e) {
				D.Error("클라이언트와의 연결통로를 여는데에 실패했습니다.");
				e.printStackTrace();
				closeAll();
			}
			
		}
		public void closeAll(){
			try {
				writer.close();
				reader.close();
				sock.close();
				D.Error(username + ", IP" + sock.getInetAddress() + "의 연결에 사용된 모든 객체를 닫습니다.");
				this.join();
			} catch (IOException | InterruptedException e) {
				// TODO Auto-generated catch block
				D.Error(username + ", IP" + sock.getInetAddress() + "의 연결에 사용된 객체를 닫는중 문제 발생");
				e.printStackTrace();
			}

		}
		public void run(){
			SocketMessage message;
			SocketMessage.MsgType type;
			try{
				while(true){
					message = (SocketMessage)reader.readObject();
					D.Debug(username + "으로 부터 메세지 도착 - " + message);
					type = message.getMsgType();
					
					switch(type){
					case NO_ACT:
						break;
					case LOGIN:
						username = serv.currentState.login(message, writer);
						break;
					case LOGOUT:
						serv.currentState.logout(message);
						break;
					case CLIENT_MSG:
						serv.currentState.act(message, writer);
						break;
					default:
						D.Error("클라이언트에서 알 수 없는 메세지 도착");
						break;
					}
				}
			}catch(Exception e){
				if(e.toString().contains("Connection reset")){
					D.Debug(username + ", IP" + sock.getInetAddress() + " 의 연결이 종료되었습니다.");
					closeAll();
				}
				else{
					D.Error(e);
					e.printStackTrace();
				}
			}
		}
	}
	@Override
	public void setUp() {
		 ServerSocket serverSocket;
		try {
			serverSocket = new ServerSocket(serv.socket);
			serv.threadConnectionWaiting = new Thread(new ServerHandler(serverSocket));
			serv.threadConnectionWaiting.start();
			
			serv.currentState = serv.serverRunState; //상태변환
			D.Debug("서버가 작동되었습니다.");
		} catch (Exception e) {
			D.Error("서버 생성 중에 이상발생");
			D.Error(e);
		}
	}

	@Override
	public void setDown() {
		D.Debug("서버가 대기중일때는 멈출 수 없습니다.");
	}

	@Override
	public void addAct(MessageType msg, SocketAct act) {
		// TODO Auto-generated method stub
		serv.MessageTypes.put(msg, act);
		D.Debug(msg + "에 해당하는 행동이 서버에 할당되었습니다. 행동 수 : " + serv.MessageTypes.size());
	}

	@Override
	public void removeAct(MessageType msg) {
		// TODO Auto-generated method stub
		serv.MessageTypes.remove(msg);
		D.Debug(msg + "에 해당하는 행동이 서버에서 제거되었습니다. 행동 수 : " + serv.MessageTypes.size());
	}

	@Override
	public synchronized String login(SocketMessage message, ObjectOutputStream writer) {
		D.Debug("서버가 대기중일때는 로그인 할 수 없습니다.");
		return null;
	}

	@Override
	public synchronized void logout(SocketMessage message) {
		D.Debug("서버가 대기중일때는 로그아웃 할 수 없습니다.");
	}

	@Override
	public synchronized void act(SocketMessage message, ObjectOutputStream writer) {
		D.Debug("서버가 대기중일때는 작동할 수 없습니다.");
	}
}
