package pachalen.socketconnection.server;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import pachalen.javadebug.JavaDebugTool;
import pachalen.socketconnection.MessageType;
import pachalen.socketconnection.SocketAct;
import pachalen.socketconnection.SocketMessage;

public class ServerRunState implements ServerState{
	private JavaDebugTool D;
	SocketServer serv = SocketServer.INST;
	
	public ServerRunState(){
		try {
			D = new JavaDebugTool("ServerRunState");
		} catch (Exception e) {}
	}

	@Override
	public void setUp() {
		// TODO Auto-generated method stub
		D.Debug("서버가 동작중일때는 시작할 수 없습니다.");
	}

	@Override
	public void setDown() {
		// TODO Auto-generated method stub
		try {
			serv.serverSocket.close();
			serv.currentState = serv.serverWaitState;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			D.Error("종료중에 이상 발생");
			e.printStackTrace();
		}
	}

	@Override
	public void addAct(MessageType msg, SocketAct act) {
		D.Debug("서버가 동작중일때는 추가할 수 없습니다.");
	}

	@Override
	public void removeAct(MessageType msg) {
		D.Debug("서버가 동작중일때는 제거할 수 없습니다.");
	}

	@Override
	public synchronized String login(SocketMessage message, ObjectOutputStream writer) {
		try{
			if(serv.clientOutputStreams.containsKey(message.getSender())){
				writer.writeObject(new SocketMessage(SocketMessage.MsgType.LOGIN_FAILURE,"Server","유저이름 중복",null));
			}
			else{
				serv.clientOutputStreams.put(message.getSender(), writer);
				D.Debug("Username : " + message.getSender() + " 가 로그인되었습니다.");
				sendUserList();
				return message.getSender();
			}
			
		}catch(Exception e){
			D.Error("로그인 중에 오류발생");
			e.printStackTrace();
		} 
		return null;
	}

	@Override
	public synchronized void logout(SocketMessage message) {
		serv.clientOutputStreams.remove(message.getSender());
		D.Debug("Username : " + message.getSender() + " 가 로그아웃되었습니다.");
		sendUserList();
	}

	@Override
	public synchronized void act(SocketMessage message, ObjectOutputStream writer) {
		if(serv.MessageTypes.containsKey(message.getDetailMsgType())){
			serv.MessageTypes.get(message.getDetailMsgType()).act(message);
		}
		else{
			D.Error("메세지 타입에 대응하는 행동이 존재하지 않습니다.");
		}
	}
	public void sendUserList(){
		String userlist = "";
		Set<String> s = serv.clientOutputStreams.keySet();
		Iterator<String> it = s.iterator();
		
		while(it.hasNext()){
			String user = it.next();
			userlist += user;
			if(it.hasNext()) userlist += "/";
		}
		
		it = s.iterator();
		String user;
		SocketMessage message = new SocketMessage(SocketMessage.MsgType.LOGIN_LIST,userlist,null, null);
		while(it.hasNext()){
			user = it.next();
			try{
				ObjectOutputStream writer = serv.clientOutputStreams.get(user);
				message.setReceiver(user);
				writer.writeObject(message);
				writer.flush();
			}
			catch(Exception e){
				D.Error(user + "클라이언트로 메세지를 보내는 중 오류발생 - " + e);
				e.printStackTrace();
				return;
			}
		}
		D.Debug("연결된 모든 클라이언트로 리스트 전송 - " + message);
	}


}
