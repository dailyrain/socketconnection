package pachalen.socketconnection.server;

import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;

import pachalen.socketconnection.MessageType;
import pachalen.socketconnection.SocketAct;
import pachalen.socketconnection.SocketMessage;

/**
 * 서버의 동작을 선언하는 인터페이스<br>
 * SetUP - 서버를 실행한다<br>
 * SetDown - 서버를 멈춘다<br>
 * 2013. 6. 4. 제작<br>
 * 순천향대학교 의료IT공학과 MSDL & Team Pachalen<br>
 * @author 최선욱(Niklane)
 * @version 1.0
 *
 */
public interface ServerState {
	void setUp();
	void setDown();
	void addAct(MessageType msg, SocketAct act);
	void removeAct(MessageType msg);
	String login(SocketMessage message, ObjectOutputStream writer);
	void logout(SocketMessage message);
	void act(SocketMessage message, ObjectOutputStream writer);
}