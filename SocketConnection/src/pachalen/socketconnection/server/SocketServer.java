package pachalen.socketconnection.server;

import java.io.ObjectOutputStream;
import java.net.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import pachalen.javadebug.JavaDebugTool;
import pachalen.socketconnection.MessageType;
import pachalen.socketconnection.SocketAct;
import pachalen.socketconnection.SocketContent;
import pachalen.socketconnection.SocketMessage;

/**
 * 소켓통신의 서버입니다.<br>
 * 싱글톤기법을 이용하여 제작되었으며 사용시에는<br>
 * SocketServer serv = SocketServer.serv;<br>
 * 를 사용해 인스턴스를 획득합니다.
 * 2013. 6. 9. 제작<br>
 * 순천향대학교 의료IT공학과 MSDL & Team Pachalen<br>
 * @author 최선욱(Niklane)
 * @version 1.0
 *
 */
public enum SocketServer {
	/**
	 * SocketServer의 Singleton Instance
	 */
	INST; //고유한 인스턴스

	int socket;
	ServerSocket serverSocket;
	Thread threadConnectionWaiting;
	JavaDebugTool D;
	HashMap<MessageType, SocketAct> MessageTypes = new HashMap<MessageType, SocketAct>();
	HashMap<String, ObjectOutputStream> clientOutputStreams = new HashMap<String, ObjectOutputStream>();

	//서버의 상태
	ServerState serverWaitState;
	ServerState serverRunState;
	ServerState currentState;

	/**
	 * 서버를 초기화합니다.
	 * @param socket 소켓번호
	 */
	public void init(int socket){
		if(serverWaitState == null){
			this.socket = socket;
			serverRunState = new ServerRunState();
			serverWaitState = new ServerWaitState();
			currentState = serverWaitState;
			try {
				D = new JavaDebugTool("SocketServer", true, true);
			} catch (Exception e) {}
			D.Debug("서버가 초기화 되었습니다.");
		}
		else{
			try {
				throw new Exception("이미 초기화된 서버입니다");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				D.Error(e);
			}
		}
	}

	/**
	 * 클라이언트로부터 메세지 획득시 수행할 행동을 추가합니다.
	 * @param msg {@link MessageType}을 구현한 열거형, 메세지의 종류를 정의.
	 * @param act {@link SocketAct}을 상속한 클래스, 수행할 행동을 정의.
	 */
	public void addAct(MessageType msg, SocketAct act){
		currentState.addAct(msg, act);
	}

	/** 
	 * 추가한 행동을 삭제합니다.
	 * @param msg {@link MessageType}을 구현한 열거형, 메세지의 종류를 정의.
	 */
	public void removeAct(MessageType msg){
		currentState.removeAct(msg);
	}

/**
 * 서버를 작동시킵니다.
 */
	public void setUp(){
		currentState.setUp();
	}

/**
 * 서버의 작동을 중지합니다.
 */
	public void setDown(){
		currentState.setDown();
	}

	/**
	 * 클라이언트로 메세지를 보냅니다.
	 * @param msgType {@link MessageType}을 구현한 열거형, 메세지의 종류
	 * @param sender 송신자
	 * @param receiver 수신자, 비워놓거나 ""처리시 서버에 연결된 모든 클라이언트에 전송합니다.
	 * @param content {@link SocketContent}를 상속한 클래스, 보낼 메세지를 정의.
	 */
	public void sendMessage(MessageType msgType, String sender, String receiver, SocketContent content){
		if(receiver == null) receiver = "";

		if(receiver.equals("")){
			Set<String> s = clientOutputStreams.keySet();
			Iterator<String> it = s.iterator();
			String user;
			SocketMessage message = new SocketMessage(SocketMessage.MsgType.SERVER_MSG,sender,null,msgType,content);
			while(it.hasNext()){
				user = it.next();
				try{
					ObjectOutputStream writer = clientOutputStreams.get(user);
					message.setReceiver(user);
					writer.writeObject(message);
					writer.flush();
				}
				catch(Exception e){
					D.Error(user + "클라이언트로 메세지를 보내는 중 오류발생 - " + e);
					e.printStackTrace();
					return;
				}
			}
			D.Debug("연결된 모든 클라이언트로 메세지 전송 - " + message);
		}
		else{
			try{
				ObjectOutputStream writer = clientOutputStreams.get(receiver);
				SocketMessage message = new SocketMessage(SocketMessage.MsgType.SERVER_MSG,sender,receiver,msgType,content);
				writer.writeObject(message);
				writer.flush();
				D.Debug(receiver + "클라이언트로 메세지 전송 - " + message);
			}
			catch(Exception e){
				D.Error(receiver + "클라이언트로 메세지를 보내는 중 오류발생 - " + e);
				e.printStackTrace();
			}
		}


	}
}
