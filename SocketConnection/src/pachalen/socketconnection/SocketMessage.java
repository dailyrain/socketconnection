package pachalen.socketconnection;

import java.io.Serializable;

public class SocketMessage implements Serializable {
	public enum MsgType{NO_ACT, LOGIN, LOGOUT, LOGIN_FAILURE, SERVER_MSG, LOGIN_LIST, CLIENT_MSG};
	public static final String ALL = "��ü";

	private MsgType msgType;
	private String sender;
	private String receiver;
	private MessageType detailMsgType;
	private SocketContent content;
	
	public SocketMessage(MsgType msgType, String sender, String receiver,
			MessageType detailMsgType, SocketContent content) {
		super();
		this.msgType = msgType;
		this.sender = sender;
		this.receiver = receiver;
		this.detailMsgType = detailMsgType;
		this.content = content;
	}
	
	public SocketMessage(MsgType msgType, String sender, String receiver,
			SocketContent content) {
		super();
		this.msgType = msgType;
		this.sender = sender;
		this.receiver = receiver;
		this.content = content;
		this.detailMsgType = null;
	}
	
	/**
	 * @return the msgType
	 */
	public MsgType getMsgType() {
		return msgType;
	}

	/**
	 * @param msgType the msgType to set
	 */
	public void setMsgType(MsgType msgType) {
		this.msgType = msgType;
	}

	public String getSender() {
		return sender;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public String getReceiver() {
		return receiver;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public SocketContent getContent() {
		return content;
	}

	public void setContent(SocketContent content) {
		this.content = content;
	}

	public MessageType getDetailMsgType() {
		return detailMsgType;
	}

	public void setDetailMsgType(MessageType detailMsgType) {
		this.detailMsgType = detailMsgType;
	}

	@Override
	public String toString() {
		return "SocketMessage ["
				+ (msgType != null ? "msgType=" + msgType + ", " : "")
				+ (sender != null ? "sender=" + sender + ", " : "")
				+ (receiver != null ? "receiver=" + receiver + ", " : "")
				+ (detailMsgType != null ? "detailMsgType=" + detailMsgType
						+ ", " : "")
				+ (content != null ? "content=" + content : "") + "]";
	}
}
